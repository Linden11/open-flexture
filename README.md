# Open Flexture

Make FreeCAD Part Design files from STLs

Sand box repository for FreeCAD Part Desigh file basted on STL from https://build.openflexure.org/openflexure-microscope/latest/all

I have started this just as an exercise to see what I can do and develop a processes for extrapolating parametric files from .stl's

- I would like to know if any one will find this useful?
- How we should license the generated FreeCAD files?
- Most importantly if the people at openflexure.org are comfortable with me doing this and making it public? If not I am more than happy to make this repository private or stop all together.

Thanks Linden

